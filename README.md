# READ ME
ランダムフォレストの前処理のためのプログラムです。

写真をまず見てください。

以下の順に実行してください。
1.  Classify.m  
2.  Search_Artery.m  
3.  Remove_No_Artery_File.m  
4.  Select_Artery_File.m
5.  Set_RandomBox_Posision.m
6.  Set_Matrix_for_Forest.m
